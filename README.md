# Descrizione
Questo widget consente di visualizzare uno o più calendari Google **pubblici** in una pagina HTML.  
I parametri possono essere specificati in una varietà di modi.

## Open source e documentazione

Questo progetto è open source: chiunque può scaricare i file necessari, ricreare il progetto e contribuire al suo miglioramento. Non ci sono restrizioni di licenza d'uso, ma si invita a citare che è stato realizzato dagli studenti [ASIRID.](http://asirid.it)

Tutto il materiale necessario si trova [su GitLab.](https://gitlab.com/poggiolevante/widget-calendario-google)

# Installazione

1) [Copiare i file di installazione](https://gitlab.com/poggiolevante/widget-calendario-google/-/archive/main/widget-calendario-google-main.zip?path=src) (
   dalla cartella src) in un web server:

- calendar-widget.js
- calendar-widget.html (opzionale, se si vuole una pagina standalone)
- *.css (file di stile opzionali)

## Utilizzo

Inserire nel proprio sito HTML uno o più div con class "calendar_output", con id univoco. L'ID verrà usato per prelevare
il file di configurazione con lo stesso nome. Infine includere 
(una sola volta, anche se si prevede di aggiungere più calendari) 
in fondo alla pagina le API google e il file
calendar-widget.js, come in esempio.

### Esempio

```html

<body>
<div class="calendar_output" id="test" max_days="6"></div>
<div class="calendar_output" id="test2" max_days="4"></div>
<script defer src="https://apis.google.com/js/api.js"></script>
<script defer src="calendar-widget.js"></script>
</body>
```

Il programma cercherà di caricare "test.json" per il primo calendario e "test2.json" per il secondo calendario.

## Tabella Parametri

| Parametro          | Nome        | Descrizione                                                                 |
|--------------------|-------------|-----------------------------------------------------------------------------|
| Chiave API         | key         | La chiave API utilizzata per ricevere gli eventi da Google. Obbligatorio.   |
| ID Calendario      | cid         | L'ID del calendario da cui ricevere gli eventi. Obbligatorio.               |
| Numero max. eventi | max_results | Il numero massimo di eventi da ricevere. Compreso fra 1 e 2500.             |
| Numero max. giorni | max_days    | Il numero massimo di giorni da mostrare.                                    |
| Stile              | style_file  | Il nome (relativo) del file CSS, con estensione, da applicare alla tabella. |
| Formato orario     | hour_format | Formato degli orari di inizio e fine evento nel calendario.                 |
| Formato giorno     | day_format  | Formato dei giorni nel calendario                                           |

Per la lista delle opzioni per il formato orario / giorno fare riferimento
a https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/DateTimeFormat/DateTimeFormat#options


Se in DEFAULT_DAYS ci sono più eventi di DEFAULT_RESULTS_NUMBER, mostrerà un numero di eventi pari a quest'ultimo e non
tutti i DEFAULT_DAYS

### Ordine di applicazione dei parametri

I parametri vengono prelevati in quest'ordine:

#### 1) URL della pagina

Si applica a tutti i calendari nella pagina.

##### Esempio

   ```
   http://127.0.0.1/calendar-widget.html?max_days=3&style_file=calendar-widget-style-poggio.css
   ```

#### 2) Tag nel div

##### Esempio

   ```html

<div class="calendar_output" id="test" max_days="6"></div>
   ```

#### 3) File di configurazione

Verrà letto (id specificato nell'html).json.

#### 4) Valori di default

Per alcuni parametri, come il numero di risultati, verrà usato il valore di default.

## Creare una chiave API

1) Recarsi su [Google Cloud Console](https://console.cloud.google.com/projectselector2/home/dashboard).
2) Creare un nuovo progetto od utilizzare un progetto già esistente.
3) Recarsi nella Libreria API:  
   ![](https://gitlab.com/poggiolevante/widget-calendario-google/-/raw/main/docs/1.png)
4) Cercare ed abilitare "Google Calendar API"  
   ![](https://gitlab.com/poggiolevante/widget-calendario-google/-/raw/main/docs/2.png)
5) Recarsi in "Credenziali":  
   ![](https://gitlab.com/poggiolevante/widget-calendario-google/-/raw/main/docs/3.png)
6) Crea delle credenziali di tipo "Chiave API":  
   ![](https://gitlab.com/poggiolevante/widget-calendario-google/-/raw/main/docs/4.png)
7) Copia la chiave API creata nel file di configurazione.

# AUTORI

Davide Palma e Salvatore Incarnato, marzo 2022
