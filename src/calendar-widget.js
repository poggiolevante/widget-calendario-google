// 13/09/2022 20:11

/**
 * Stampa una data nel formato specificato
 * @param date La data da stampare
 * @param options L'opzione di formattazione
 * @returns {string} La data formattata
 */
function printDate(date, options) {
    return new Date(date).toLocaleString("it-IT", options);
}

/**
 * Aggiunge il file CSS specificato al documento
 * @param filename Il nome del file CSS
 */
function addCss(filename) {
    const stylesheet = document.createElement("link");
    stylesheet.href = getBasePath() + filename;
    stylesheet.rel = "stylesheet";
    stylesheet.type = 'text/css'
    document.getElementsByTagName("head")[0].appendChild(stylesheet);
    window.___gcfg = {parsetags: 'onload'};
}

/**
 * Classe per facilitare la costruzione di una tabella.
 */
class Table {
    /**
     * Inizializza la tabella.
     */
    constructor(div) {
        this.tbl = document.createElement('table');
        // Contatore di righe, per creare gli ID
        this.rows = 0;
        // Conservare il div per poi poterci aggiungere la tabella
        this.div = div;
        // L'ID del div
        this.tid = this.div.id;
        // Inserisce l'ID del div nella classe del div
        this.div.className += " " + this.tid;
        // Da cui verranno derivati gli ID dei campi
        this.tbl.id = "calendar_table_" + this.tid;
        this.tbl.className = "calendar_table" + " " + this.tid;
    }

    /**
     * Scrive la tabella nel div conservato.
     */
    finalize() {
        this.div.appendChild(this.tbl);
    }

    /**
     * Inserisce un giorno nella tabella.
     * @param day Il giorno da inserire.
     */
    insertDay(day) {
        const tr = this.tbl.insertRow();
        tr.className = "day_tr" + " " + this.tid;
        tr.id = "day_tr" + "_" + this.tid + "_" + this.rows;
        const td = tr.insertCell();
        td.className = "day_td" + " " + this.tid;
        td.id = "day_td" + "_" + this.tid + "_" + this.rows++;
        const txtNode = document.createTextNode(day);
        td.appendChild(txtNode);
        td.setAttribute('colSpan', '3');
    }

    /**
     * Inserisce un evento nella tabella.
     * @param eventArray L'evento da inserire.
     */
    insertEvent(eventArray) {
        if (eventArray.length === 3) {
            // Inserisce per prima cosa la riga dell'evento.
            const tr = this.tbl.insertRow();
            tr.className = "event_tr" + " " + this.tid;
            tr.id = "event_tr" + "_" + this.tid + "_" + this.rows;
            // Ora d'inizio dell'evento.
            if (eventArray[0]) {
                const td = tr.insertCell();
                td.className = "event_start_td" + " " + this.tid;
                td.id = "event_start_td" + "_" + this.tid + "_" + this.rows;
                const txtNode = document.createTextNode(eventArray[0]);
                td.appendChild(txtNode);
            }
            // Ora di fine dell'evento.
            if (eventArray[1]) {
                const td = tr.insertCell();
                td.className = "event_end_td" + " " + this.tid;
                td.id = "event_end_td" + "_" + this.tid + "_" + this.rows;
                const txtNode = document.createTextNode(eventArray[1]);
                td.appendChild(txtNode);
            }
            // Nome dell'evento.
            if (eventArray[2]) {
                const td = tr.insertCell();
                td.className = "event_name_td" + " " + this.tid;
                td.id = "event_name_td" + "_" + this.tid + "_" + this.rows++;
                const txtNode = document.createTextNode(eventArray[2]);
                td.appendChild(txtNode);
            }
        } else if (eventArray.length === 1) {
            // Inserisce per prima cosa la riga dell'evento.
            const tr = this.tbl.insertRow();
            tr.className = "event_fullday_tr" + " " + this.tid;
            tr.id = "event_fullday_tr" + "_" + this.tid + "_" + this.rows;

            // Nome dell'evento.
            if (eventArray[0]) {
                const td = tr.insertCell();
                td.className = "event_fullday_name_td" + " " + this.tid;
                td.id = "event_fullday_name_td" + "_" + this.tid + "_" + this.rows++;
                td.setAttribute('colSpan', '3');
                const txtNode = document.createTextNode(eventArray[0]);
                td.appendChild(txtNode);
            }
        }
    }
}

/**
 * Ottiene una singola opzione, cercando in ordine da:
 * 1. L'URL
 * 2. Il tag del div
 * 3. Il file di configurazione
 * @param element Il div in cui stiamo eseguendo
 * @param optionName Il nome dell'opzione da cercare
 * @param config Il file di configurazione
 * @returns {string|undefined|*} L'opzione trovata, o no
 */
function getOption(element, optionName, config) {
    optionName = optionName.toLowerCase();

    // 1. L'URL
    const urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has(optionName)) {
        console.log(element.id + ": Parametro " + optionName + " trovato nell'url");
        return urlParams.get(optionName);
    }

    // 2. Il tag del div
    if (element) {
        if (element.hasAttribute(optionName)) {
            console.log(element.id + ": Parametro " + optionName + " trovato nel tag");
            return element.getAttribute(optionName);
        }
    }

    // 3. Il file di configurazione
    if (config && (optionName in config)) {
        console.log(element.id + ": Parametro " + optionName + " trovato nella config");
        return config[optionName];
    }

    return undefined;

}

/**
 * Ritorna l'URL in cui stiamo eseguendo.
 * @returns {string} L'URL in cui stiamo eseguendo.
 */
function getBasePath() {
    return decodeURI(new Error().stack.match(/([^ \n(@])*([a-z]*:\/\/\/?)*?[a-z0-9\/\\]*\.js/ig)[0]) + "/../"
}

/**
 * Scarica un file json.
 * @param url L'url del file json da scaricare.
 * @returns {Promise<undefined|any>} Una Promise che ritorna il file json scaricato.
 */
async function getJson(url) {
    try {
        const response = await fetch(url, {
            method: 'GET',
            cache: 'no-cache',
        });
        return await response.json();
    } catch (error) {

        console.error(error.message);
        return undefined;
    }
}

/**
 * Ottiene tutti i parametri di configurazione.
 * @param elem Il div in cui stiamo eseguendo.
 * @returns {Promise<{}>} Una Promise che ritorna un oggetto con tutti i parametri di configurazione.
 */
async function getOptions(elem) {
    const dic = {};

    // Lista delle opzioni con valori di default.
    const options = {
        'key': undefined,
        'cid': undefined,
        'max_results': 50,
        'max_days': 7,
        'style_file': "",
        'hour_format': {hour: 'numeric', minute: 'numeric'},
        'day_format': {year: 'numeric', month: 'long', day: 'numeric'}
    };

    // Ottiene il file di configurazione.
    const confile = await getJson(getBasePath() + elem.id + ".json");

    if (confile) {
        for (const [key, value] of Object.entries(options)) {

            // Ricerca in: URL, tag, config.
            opt = getOption(elem, key, confile);

            if (opt === undefined) {

                // Usiamo l'eventuale valore di default
                if (value === undefined) {
                    elem.append("\nErrore: parametro " + key + " non trovato\n");
                } else {
                    console.log("Using default value " + value + " for option " + key);
                    dic[key] = value;
                }

            } else {
                //getOption ci ha dato l'opzione, la usiamo.
                dic[key] = opt;
            }

        }

    }

    return dic;

}

/**
 * Si assicura che un numero sia compreso tra due numeri.
 * @param n Il numero da controllare.
 * @param min Il minimo.
 * @param max Il massimo.
 * @param def Il valore di default.
 * @returns {number|*} Il numero corretto.
 */
function ensure_range(n, min, max, def) {
    n = parseInt(n, 10)
    if (!isNaN(n)) {
        if (n < min) {
            return min;
        } else if (n > max) {
            return max;
        } else {
            return n;
        }
    } else {
        return def;
    }
}

/**
 * La funzione principale, crea un calendario e lo stampa nel div passato.
 * @param cal Il div in cui stiamo eseguendo.
 * @returns {Promise<void>}
 */
async function createCalendar(cal) {
    if (cal.id === "") {
        cal.append("Sono in un div senza id");
        return;
    }
    const options = await getOptions(cal);
    if (options.key === undefined) {
        cal.append(cal.id + " non ha trovato la chiave per l'API");
        return;
    }
    if (options.cid === undefined) {
        cal.append(cal.id + " non ha trovato l'id del calendario");
        return;
    }
    options["max_results"] = ensure_range(options["max_results"], 1, 2500, 50);
    options["max_days"] = ensure_range(options["max_days"], 1, 100, 7);

    // Lista delle API google del calendario
    const DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];
    // 1) Inizializza la libreria client Javascript
    await gapi.client.init({
        apiKey: options["key"],
        discoveryDocs: DISCOVERY_DOCS
        // 2) Esegue la funzione get_events() che scarica gli eventi e li raggruppa
    }).then(() => {
        try {

            // Get the list of events from the calendar
            return gapi.client.calendar.events.list({
                // Unico parametro richiesto
                calendarId: options["cid"],
                // Non ricevere la lista dei partecipanti
                maxAttendees: 1,
                // Numero massimo di eventi da ricevere (max 2500)
                maxResults: options["max_results"],
                // Ordina per data avvio evento
                orderBy: 'startTime',
                // Ricevi gli eventi ricorrenti come
                // eventi singoli e ripetuti
                singleEvents: true,
                // Ricevi solo gli eventi presenti-futuri
                timeMin: new Date().toISOString()
            }).then(function (response) {

                const events = response.result.items;

                // Raggruppamento degli eventi per giorno d'inizio

                // Lista che contiene i vari gruppi
                let groups = [];
                let tempList = [];
                let lastDate = new Date().noTime();

                events.forEach(event => {

                    let startDate;
                    if (event.start.date) {

                        // L'evento dura l'intera giornata
                        startDate = new Date(event.start.date).noTime();

                    } else {

                        startDate = new Date(event.start.dateTime).noTime();

                    }

                    // Se la data d'inizio è uguale, aggiungi l'evento al gruppo esistente
                    if (startDate.getTime() === lastDate.getTime()) {

                        tempList.push(event)

                        // Se la data d'inizio è diversa, crea un nuovo gruppo e aggiungi l'evento al nuovo gruppo
                    } else {

                        // Finalizza il vecchio gruppo e aggiungi alla lista di gruppi
                        if (tempList.length !== 0) groups.push(tempList);

                        tempList = [];
                        tempList.push(event);

                        lastDate = startDate;

                    }
                })
                // Aggiungi l'ultimo gruppo alla lista di gruppi
                if (tempList.length !== 0) groups.push(tempList);

                return groups;

            });
        } catch (e) {

            console.error(e);
            document.body.append(e);

        }
    }, err => {
        console.log(err);
        cal.append(err.error.message);
        // 3) Crea una tabella HTML con i gruppi e la stampa
    }).then(groups => {
        const table = new Table(cal);

        // Vengono considerati max n giorni
        groups.slice(0, options["max_days"]).forEach(gruppo => {

            if (gruppo[0]) {

                // Inserisce la data d'inizio del gruppo degli eventi nella tabella
                if (gruppo[0].start.date) {
                    table.insertDay(printDate(gruppo[0].start.date, options["day_format"]));
                } else if (gruppo[0].start.dateTime) {
                    table.insertDay(printDate(gruppo[0].start.dateTime, options["day_format"]));
                }

                gruppo.forEach(evento => {

                    // Per ogni evento del gruppo, inseriscilo nella tabella
                    if (evento.start.date) {
                        table.insertEvent([
                            evento.summary
                        ])
                    } else if (evento.start.dateTime) {
                        table.insertEvent([
                            printDate(evento.start.dateTime, options["hour_format"]),
                            printDate(evento.end.dateTime, options["hour_format"]),
                            evento.summary
                        ])

                    }

                })

            }

        })

        // Stampa in output la tabella
        table.finalize()

        // 4) Aggiunge il CSS
    }, (err) => {
        console.log(err.body);
        cal.append(err.body);
    }).then(() => {
        addCss(options["style_file"])
    });
}

async function createCalendars(cals) {
    for (let cal of cals) {
        await createCalendar(cal);
    }
}
document.addEventListener('DOMContentLoaded', () => {
    // Questo ci permette di chiamare noTime() su un oggetto Date
    // e di avere solo la data ignorando l'ora e timezone
    Date.prototype.noTime = function () {
        const d = new Date(this);
        d.setHours(0, 0, 0, 0);
        return d;
    }
    // For each div that has class calendar_output
    const cals = document.getElementsByClassName('calendar_output');

    gapi.load('client', async () => {
        await createCalendars(cals);
    });
});

